Notes
=====
The manim-automata plugin currently relies on JFLAP files, future updates will enable the user to create automata without JFLAP.
[JFLAP](https://www.jflap.org) is software for experimenting with formal languages topics.

Installation
============
To install manim-automata plugin run:

   pip install manim-automata

To see which version of manim-automata you have:

    manim-automata

or

    pip list


Importing
=========
To use manim-automata in your project, you can:

* Add ``from manim_automata import *`` to your script.
Once manim-automata has been imported, you can use the ManimAutomata class to create automata.

How To Use
==========


